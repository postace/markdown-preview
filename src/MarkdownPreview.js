import React from "react";
import marked from "marked";

import "./Markdown.css";

marked.setOptions({
  breaks: true
});

const renderer = new marked.Renderer();
renderer.link = function(href, title, text) {
  return `<a target="_blank" href="${href}">${text}</a>`;
};

const DEFAULT_CONTENT = `
# Welcome to my React Markdown Previewer!

## This is a sub-heading...
### And here's some other cool stuff:
  
Heres some code, \`<div></div>\`, between 2 backticks.

\`\`\`
// this is multi-line code:

function anotherExample(firstLine, lastLine) {
  if (firstLine == '\`\`\`' && lastLine == '\`\`\`') {
    return multiLineCode;
  }
}
\`\`\`
  
You can also make text **bold**... whoa!
Or _italic_.
Or... wait for it... **_both!_**
And feel free to go crazy ~~crossing stuff out~~.

There's also [links](https://www.freecodecamp.com), and
> Block Quotes!

And if you want to get really crazy, even tables:

Wild Header | Crazy Header | Another Header?
------------ | ------------- | ------------- 
Your content can | be here, and it | can be here....
And here. | Okay. | I think we get it.

- And of course there are lists.
  - Some are bulleted.
     - With different indentation levels.
        - That look like this.


1. And there are numbererd lists too.
1. Use just 1s if you want! 
1. But the list goes on...
- Even if you use dashes or asterisks.
* And last but not least, let's not forget embedded images:

![React Logo w/ Text](https://goo.gl/Umyytc)
`;

const Editor = props => (
  <textarea
    id="editor"
    value={props.content}
    onChange={props.onChange}
    style={{
      backgroundColor: "#c5e1a5",
      width: "650px",
      height: "500px"
    }}
  />
);

const Preview = props => (
  <div
    style={{ backgroundColor: "#b2ebf2" }}
    id="preview"
    dangerouslySetInnerHTML={{
      __html: marked(props.content, { renderer: renderer })
    }}
  />
);

class MarkdownPreview extends React.Component {
  state = {
    content: DEFAULT_CONTENT
  };

  handleChange = e => {
    this.setState({ content: e.target.value });
  };

  render() {
    const { content } = this.state;

    return (
      <div style={{ display: "flex", flexDirection: "row" }}>
        <div
          style={{
            flex: 1,
            marginTop: "20px",
            marginLeft: "20px"
          }}
        >
          <h3>Editor</h3>
          <Editor content={content} onChange={this.handleChange} />
        </div>

        <div style={{ flex: 1, marginTop: "20px" }}>
          <h3>Preview</h3>
          <Preview content={content} />
        </div>
      </div>
    );
  }
}

export default MarkdownPreview;
